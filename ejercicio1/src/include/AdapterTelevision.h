#pragma once
#include <iostream>
#include "TelevisorAnalogico.h"
#include "TelevisorDigital.h"

class AdapterTelevisionHeader : public TelevisorDigital, public TelevisorAnalogico{
    public:
        virtual ~AdapterTelevisionHeader() = default;
        void recibirSenalAnalogica(std::string sennal[]);
        void recibirSenalDigital(std::string sennal[]);
};
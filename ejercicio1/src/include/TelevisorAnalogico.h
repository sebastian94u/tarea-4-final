#pragma once
#include <iostream>
#include <string>

class TelevisorAnalogico
{
    public:        
        virtual ~TelevisorAnalogico() = default;
        void recibirSenalAnalogica(std::string sennal[]);
};
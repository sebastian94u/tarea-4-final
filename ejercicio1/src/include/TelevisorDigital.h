#pragma once
#include <iostream>
#include <string>

class TelevisorDigital
{
    public:
        virtual ~TelevisorDigital() = default;
        void recibirSenalDigital(std::string sennal[]);
};
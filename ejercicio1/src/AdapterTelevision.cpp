#include <iostream>
#include <string>
#include <vector>
#include "AdapterTelevision.h"

class AdapterTelevision : public AdapterTelevisionHeader
{
    protected:
        const std::string sennal_analogica[16] = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15"};
    public:
        void recibirSenalAnalogica(std::string sennal[]);
        void recibirSenalDigital(std::string sennal[]);
};

void AdapterTelevisionHeader::recibirSenalAnalogica(std::string sennal[])
{    
    std::vector<std::string> paquete_analogico;
    for (size_t i = 0; i < sennal->length(); i++)
    {
        std::string unidad_paquete = sennal[i];
        int unidad_paquete_convertido = std::stoi(unidad_paquete, nullptr, 2);
        paquete_analogico.push_back(std::to_string(unidad_paquete_convertido));
        std::cout << paquete_analogico[i];
    }
    
}

void AdapterTelevisionHeader::recibirSenalDigital(std::string sennal[]) { }
#include <iostream>
#include <string>
#include <vector>
#include <random>
#include <algorithm>
#include "Emisor.h"

class Emisor : public EmisorHeader {
    protected:
        const std::string sennal_digital[16] = {"0000", "0001", "0010", "0011", "0100", "0101", "0110", "0111", "1000", "1001", "1010", "1011", "1100", "1101", "1110", "1111"};
    public:
        std::vector<std::string> emitirSennal();
};

std::vector<std::string> Emisor::emitirSennal()
{
    std::vector<std::string> paquete_sennal;
    for (size_t i = 0; i < 4; i++)
    {
        std::random_device rd;
        std::mt19937 generator(rd());
        std::uniform_int_distribution<int> distribution(0, 15);
        int randomNumber = distribution(generator);
        paquete_sennal.push_back(sennal_digital[randomNumber]);
        std::cout << paquete_sennal[i] << " ";
    }    
    return paquete_sennal;    
}

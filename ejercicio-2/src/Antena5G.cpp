#include "Antena5G.h"

#include <iostream>

bool Antena5G::GetEstadoAntena() const {
  return estado_antena;
}

void Antena5G::SetEstadoAntena() {
  estado_antena = !estado_antena;
}

void Antena5G::UsarAntena() {
  std::cout << "Usando antena 5G..." << std::endl;
}

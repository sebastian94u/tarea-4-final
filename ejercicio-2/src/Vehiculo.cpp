#include "Vehiculo.h"

#include <iostream>
#include <string>

Vehiculo::Vehiculo(Antenas* antena) : antena_(antena) {}

Vehiculo::~Vehiculo() {}

void Vehiculo::CreacionVehiculo() {
  std::cout << "Creando vehiculo." << std::endl;
}

void Vehiculo::BuscarConexion() {
  this->antena_->GetEstadoAntena();
}

void Vehiculo::CambiarEstadoAntena() {
  this->antena_->SetEstadoAntena();
}

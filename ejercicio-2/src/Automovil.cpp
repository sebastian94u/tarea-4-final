#include "Automovil.h"
#include <iostream>

Automovil::Automovil(Antenas* antena) : Vehiculo(antena) {}

void Automovil::CreacionVehiculo() {
  std::cout << "Automovil de " << kCuatroLlantas << " llantas creado" << std::endl;
}

void Automovil::BuscarConexion() {
  this->antena_->UsarAntena();
}

void Automovil::CambiarEstadoAntena() {
  this->antena_->SetEstadoAntena();
}

#include "AntenaWAN.h"
#include <iostream>

bool AntenaWAN::GetEstadoAntena() const {
  return estado_antena;
}

void AntenaWAN::SetEstadoAntena() {
  estado_antena = !estado_antena;
}

void AntenaWAN::UsarAntena() {
  std::cout << "Usando antena WAN..." << std::endl;
}

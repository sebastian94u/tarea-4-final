#include "Motocicleta.h"
#include <iostream>

Motocicleta::Motocicleta(Antenas* antena) : Vehiculo(antena) {}

void Motocicleta::CreacionVehiculo() {
  std::cout << "Motocicleta de " << kCDosLlantas << " llantas creada" << std::endl;
}

void Motocicleta::BuscarConexion() {
  this->antena_->UsarAntena();
}

void Motocicleta::CambiarEstadoAntena() {
  this->antena_->SetEstadoAntena();
}

#ifndef MOTOCICLETA_H
#define MOTOCICLETA_H

#include "Vehiculo.h"

class Motocicleta : public Vehiculo {
  private:
    const int kCDosLlantas = 2;
  public:
    Motocicleta(Antenas* antena);
    void CreacionVehiculo() override;
    void BuscarConexion() override;
    void CambiarEstadoAntena() override;
};

#endif
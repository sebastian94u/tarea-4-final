#ifndef ANTENAWAN_H
#define ANTENAWAN_H

#include "Antenas.h"

class AntenaWAN : public Antenas {
  private:
    bool estado_antena;
  public:
    bool GetEstadoAntena() const override;
    void SetEstadoAntena() override;
    void UsarAntena() override;
};

#endif

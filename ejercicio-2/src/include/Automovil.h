#ifndef AUTOMOVIL_H
#define AUTOMOVIL_H

#include "Vehiculo.h"

class Automovil : public Vehiculo {
  private:
    const int kCuatroLlantas = 4;
  public:
    Automovil(Antenas* antena);
    void CreacionVehiculo() override;
    void BuscarConexion() override;
    void CambiarEstadoAntena() override;
};

#endif
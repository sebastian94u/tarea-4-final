#ifndef ANTENA5G_H
#define ANTENA5G_H

#include "Antenas.h"

class Antena5G : public Antenas {
  private:
    bool estado_antena;
  public:
    bool GetEstadoAntena() const override;
    void SetEstadoAntena() override;
    void UsarAntena() override;
};

#endif

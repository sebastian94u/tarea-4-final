#ifndef VEHICULO_H
#define VEHICULO_H

#include "Antenas.h"

#include <iostream>

class Vehiculo {
  protected:
    Antenas* antena_;
  public:
    Vehiculo(Antenas* antena);
    virtual ~Vehiculo();
    virtual void CreacionVehiculo();
    virtual void BuscarConexion();
    virtual void CambiarEstadoAntena();
};

#endif

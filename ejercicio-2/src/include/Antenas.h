#ifndef ANTENAS_H
#define ANTENAS_H

class Antenas {
  public:
    virtual ~Antenas() {};
    virtual bool GetEstadoAntena() const = 0;
    virtual void SetEstadoAntena() = 0;
    virtual void UsarAntena() = 0;
};

#endif

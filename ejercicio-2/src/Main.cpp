#include <iostream>
#include "Antenas.h"
#include "AntenaWAN.h"
#include "Antena5G.h"
#include "Vehiculo.h"
#include "Automovil.h"
#include "Motocicleta.h"

void CrearVehiculo();

int main()
{
  CrearVehiculo();

  return 0;
}

void CrearVehiculo()
{
  int opcion;
  bool esta_adentro = true;
  Vehiculo *vehiculo = nullptr;
  Antenas *antena = nullptr;

  while (esta_adentro)
  {
    std::cout << "Seleccionar vehiculo a crear:" << std::endl;
    std::cout << "1. Automovil" << std::endl;
    std::cout << "2. Motocicleta" << std::endl;
    std::cout << "0. Exit" << std::endl;
    std::cout << "Numero opcion: ";
    std::cin >> opcion;

    if (opcion == 1)
    {
      // Crear auto
      int tipo_antena;
      std::cout << "Seleccionar tipo de antena: " << std::endl;
      std::cout << "1. AntenaWAN" << std::endl;
      std::cout << "2. Antena 5G" << std::endl;
      std::cout << "Numero opcion: ";
      std::cin >> tipo_antena;
      if (tipo_antena == 1)
      {
        antena = new AntenaWAN();
        vehiculo = new Automovil(antena);
      }
      else if (tipo_antena == 2)
      {
        antena = new Antena5G();
        vehiculo = new Automovil(antena);
      }
    }
    else if (opcion == 2)
    {
      // Crear moto
      int tipo_antena;
      std::cout << "Seleccionar tipo de antena: " << std::endl;
      std::cout << "1. AntenaWAN" << std::endl;
      std::cout << "2. Antena 5G" << std::endl;
      std::cout << "Numero opcion: ";
      std::cin >> tipo_antena;
      if (tipo_antena == 1)
      {
        antena = new AntenaWAN();
        vehiculo = new Motocicleta(antena);
      }
      else if (tipo_antena == 2)
      {
        antena = new Antena5G();
        vehiculo = new Motocicleta(antena);
      }
    }
    else if (opcion == 0)
    {
      // Salir del menu
      esta_adentro = false;
    }
    else
    {
      std::cout << "Invalid choice. Please try again." << std::endl;
    }
    if (vehiculo != nullptr)
    {
      // Ver tipo de antena para el tipo de vehiculo ya sea automovil o moto
      std::cout << "------------------------------" << std::endl;
      vehiculo->CreacionVehiculo();
      vehiculo->BuscarConexion();
      std::cout << "------------------------------" << std::endl;

      delete vehiculo;
    }
  }
}

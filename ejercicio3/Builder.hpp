#ifndef BUILDER_HPP
#define BUILDER_HPP

class Builder {
public:
    virtual void buildButtons() = 0;
    virtual void buildLights() = 0;
    virtual void buildPorts() = 0;
    virtual void buildMemory() = 0;
};

class LaunchPadBuilder : public Builder {
public:
    void buildButtons() override;
    void buildLights() override;
    void buildPorts() override;
    void buildMemory() override;
};

#endif  // BUILDER_HPP

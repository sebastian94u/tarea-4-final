#ifndef PROTOTYPE_HPP
#define PROTOTYPE_HPP

class Prototype {
public:
    virtual Prototype* clone() = 0;
};

class ConcretePrototype : public Prototype {
public:
    Prototype* clone() override;
};

#endif 

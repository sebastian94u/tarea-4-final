// Dentro de Facade.cpp

/*


*/

#include "Facade.hpp"
#include "Builder.hpp"
#include "Prototype.hpp"

#include <iostream>

Facade::Facade() {
    builder_ = new LaunchPadBuilder();
    prototype_ = new ConcretePrototype();
}

void Facade::createLaunchpad() {
    builder_->buildButtons();
    builder_->buildLights();
    builder_->buildPorts();
    builder_->buildMemory();
    // Lógica adicional para crear el launchpad

    
    
    std::cout << "Se ha creado el launchpad." << std::endl; 
    
    
}

void Facade::createCopy() {
    Prototype* clonedPrototype = prototype_->clone();
    // Lógica para crear una copia del launchpad utilizando el prototipo clonado
    

   std::cout << "Se ha creado una copia del launchpad." << std::endl;
}

void Facade::testSynchronization() {
    // Lógica para probar la sincronización con el módulo Tester

    
    
    std::cout << "Se ha realizado la prueba de sincronización." << std::endl;
}

void Facade::exportAudioMix() {
    // Lógica para exportar la mezcla de audio utilizando el Audio Mixer, Codec, etc.

    
    
    
    std::cout << "Se ha exportado la mezcla de audio." << std::endl;
}




/*

#include "Facade.hpp"
#include "Builder.hpp"
#include "Prototype.hpp"


Facade::Facade() {
    builder_ = new LaunchPadBuilder();
    prototype_ = new ConcretePrototype();
}

void Facade::createLaunchpad() {
    builder_->buildButtons();
    builder_->buildLights();
    builder_->buildPorts();
    builder_->buildMemory();
   
}

void Facade::createCopy() {
    Prototype* clonedPrototype = prototype_->clone();
    
}

void Facade::testSynchronization() {
    
}

void Facade::exportAudioMix() {
    
}

*/
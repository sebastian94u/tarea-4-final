#ifndef FACADE_HPP
#define FACADE_HPP

#include "Builder.hpp"
#include "Prototype.hpp"

class Facade {
public:
    Facade();
    void createLaunchpad();
    void createCopy();
    void testSynchronization();
    void exportAudioMix();

private:
    Builder* builder_;
    Prototype* prototype_;
};

#endif  // FACADE_HPP




#include "Facade.hpp"
#include "Tester.hpp"
#include "AudioMixer.hpp"
#include "Codec.hpp"
#include "CompressorCodec.hpp"
#include "AudioWriter.hpp"
#include "FrequencyFilter.hpp"

int main() {
    // Crear instancias y utilizar los patrones de diseño según sea necesario
    Facade facade;
    facade.createLaunchpad();
    facade.createCopy();
    facade.testSynchronization();
    facade.exportAudioMix();

    Tester tester;
    tester.linkLaunchpads();
    tester.runSoundTests();
    tester.exportMixes();

    AudioMixer audioMixer;
    audioMixer.mixAudio();

    Codec codec;
    codec.encode();

    CompressorCodec compressorCodec;
    compressorCodec.compress();

    AudioWriter audioWriter;
    audioWriter.writeAudio();

    FrequencyFilter frequencyFilter;
    frequencyFilter.filterFrequency();

    // ...

    return 0;
}

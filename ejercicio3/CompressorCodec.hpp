#ifndef COMPRESSORCODEC_HPP
#define COMPRESSORCODEC_HPP

#include "Codec.hpp"

class CompressorCodec : public Codec {
public:
    void compress();
};

#endif  // COMPRESSORCODEC_HPP

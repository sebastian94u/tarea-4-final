#ifndef TESTER_HPP
#define TESTER_HPP

class Tester {
public:
    void linkLaunchpads();
    void runSoundTests();
    void exportMixes();
};

#endif  // TESTER_HPP
